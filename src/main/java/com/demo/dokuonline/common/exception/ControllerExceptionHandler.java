package com.demo.dokuonline.common.exception;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonParseException;

@ControllerAdvice
@ResponseBody
public class ControllerExceptionHandler {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();

		ex.getBindingResult().getAllErrors().forEach((error) -> {
			logger.info("Response Code ::: " + error.getDefaultMessage() + " FieldName ::: "
					+ ((FieldError) error).getField());
			String responseCode = "responseCode";
			String errorMessage = error.getDefaultMessage();
			errors.put(responseCode, errorMessage);
		});
		return errors;
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(JsonParseException.class)
	public Map<String, String> handleValidationExceptions(JsonParseException ex) {
		Map<String, String> errors = new HashMap<>();
		logger.info("Response Code ::: ", ex.getMessage());
		String fieldName = "responseCode";
		String errorMessage = ex.getMessage();
		errors.put(fieldName, errorMessage);
		return errors;
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public Map<String, String> handleValidationExceptions(HttpMessageNotReadableException ex) {
		Map<String, String> errors = new HashMap<>();
		logger.info("Response Code ::: ", ex.getMessage());
		String fieldName = "responseCode";
		String errorMessage = ex.getMessage();
		errors.put(fieldName, errorMessage);
		return errors;
	}
}
