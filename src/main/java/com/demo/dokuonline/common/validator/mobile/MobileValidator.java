package com.demo.dokuonline.common.validator.mobile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MobileValidator implements ConstraintValidator<Mobile, Long> {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		return value < 999999999;
	}
}
