package com.demo.dokuonline.dummy.notification.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dokuonline.employee.endpoint.model.EmployeeModel;

@RestController
@CrossOrigin
@RequestMapping("/api/dummy/notification")
public class DummyNotification {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PostMapping("/message")
	public @ResponseBody String greeting(@RequestBody EmployeeModel data) {
		return "Welcome, " + data.getFullName() + "!";
	}
}
