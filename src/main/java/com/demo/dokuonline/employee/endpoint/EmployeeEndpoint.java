package com.demo.dokuonline.employee.endpoint;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dokuonline.employee.endpoint.model.EmployeeModel;
import com.demo.dokuonline.employee.service.EmployeeService;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/employee")
public class EmployeeEndpoint {

	@Autowired
	EmployeeService employeeService;

	@PostMapping("/create")
	public @ResponseBody EmployeeModel create(@Valid @RequestBody EmployeeModel data) {
		return employeeService.create(data);
	}

	@PutMapping("/create")
	public @ResponseBody EmployeeModel update(@Valid @RequestBody EmployeeModel data) {
		return employeeService.create(data);
	}

	@GetMapping("/all")
	public List<EmployeeModel> findAll() {
		return employeeService.findAll();
	}

	@GetMapping("/{id}")
	public EmployeeModel findById(@PathVariable String id) {
		return employeeService.findById(id);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable String id) {
		employeeService.delete(id);
	}
}
