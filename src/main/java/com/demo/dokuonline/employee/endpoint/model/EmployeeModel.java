package com.demo.dokuonline.employee.endpoint.model;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

public class EmployeeModel {
	private String id;
	@NotBlank(message = "The fullName is a mandatory field")
	private String fullName;
	@NotNull(message = "The mobileNumber is a mandatory field")
	@Length(min = 11, max = 13, message = "The mobileNumber should be in a valid length")
	private String mobileNumber;
	@NotBlank(message = "The emailId is a mandatory field")
	@Email(message = "The emailId should be in a valid email format")
	private String emailId;
	@NotNull(message = "The dateOfBirth is a mandatory field")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate dateOfBirth;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
