package com.demo.dokuonline.employee.service;

import java.util.List;

import com.demo.dokuonline.employee.endpoint.model.EmployeeModel;

public interface EmployeeService {
	EmployeeModel create(EmployeeModel data);

	EmployeeModel update(EmployeeModel data);

	List<EmployeeModel> findAll();

	EmployeeModel findById(String id);

	void delete(String id);
}
