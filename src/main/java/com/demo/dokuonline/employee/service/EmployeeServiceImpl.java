package com.demo.dokuonline.employee.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dokuonline.employee.endpoint.model.EmployeeModel;
import com.demo.dokuonline.employee.entity.EmployeeEntity;
import com.demo.dokuonline.employee.repository.EmployeeRepository;
import com.demo.dokuonline.integration.notification.repository.NotificationService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	NotificationService notifService;

	@Override
	public EmployeeModel create(EmployeeModel data) {
		// TODO Auto-generated method stub
		EmployeeEntity entity = new EmployeeEntity();
		BeanUtils.copyProperties(data, entity);
		entity = employeeRepository.save(entity);

		BeanUtils.copyProperties(entity, data);
		logger.info("[{}]", notifService.greeting(data));
		return data;
	}

	@Override
	public EmployeeModel update(EmployeeModel data) {
		// TODO Auto-generated method stub
		EmployeeEntity entity = new EmployeeEntity();
		BeanUtils.copyProperties(data, entity);
		entity = employeeRepository.save(entity);

		BeanUtils.copyProperties(entity, data);
		return data;
	}

	@Override
	public List<EmployeeModel> findAll() {
		// TODO Auto-generated method stub
		Iterable<EmployeeEntity> findAll = employeeRepository.findAll();

		List<EmployeeModel> listEmp = new ArrayList<EmployeeModel>();
		listEmp.forEach(data -> {
			EmployeeModel model = new EmployeeModel();
			BeanUtils.copyProperties(data, model);
			listEmp.add(model);
		});
		return listEmp;
	}

	@Override
	public EmployeeModel findById(String id) {
		// TODO Auto-generated method stub
		Optional<EmployeeEntity> emp = employeeRepository.findById(id);
		EmployeeModel model = new EmployeeModel();
		emp.ifPresent(data -> {
			BeanUtils.copyProperties(data, model);
		});
		return model;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		employeeRepository.deleteById(id);
	}

}
