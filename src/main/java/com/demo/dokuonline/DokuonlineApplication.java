package com.demo.dokuonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DokuonlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(DokuonlineApplication.class, args);
	}

}
