package com.demo.dokuonline.integration;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

@Component
public class RequestSignatureInterceptor implements ClientHttpRequestInterceptor {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		// TODO Auto-generated method stub
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss O");

		String date = dtf.format(ZonedDateTime.now(ZoneOffset.UTC));

//		request.getHeaders().remove("Accept");
//		request.getHeaders().remove("Content-Length");

		this.logRequest(request, body);

		return execution.execute(request, body);
	}

	private void logRequest(HttpRequest request, byte[] body) throws IOException {
		boolean infoEnabled = logger.isInfoEnabled();
		if (infoEnabled) {
			String line1 = "\n================================================request begin==================================================\n";
			String line2 = "URI          : [{}]\n";
			String line3 = "Method       : [{}]\n";
			String line4 = "Accept       : {}\n";
			String line5 = "Content Type : [{}]\n";
			String line6 = "=================================================request end====================================================\n";

			String logPattern = line1 + line2 + line3 + line4 + line5 + line6;

			logger.info(logPattern, request.getURI(), request.getMethod(), request.getHeaders().getAccept(),
					request.getHeaders().getContentType());

//			logger.info("===========================request begin================================================");
//			logger.info("URI          : {}", request.getURI());
//			logger.info("Method       : {}", request.getMethod());
//			logger.info("Headers      : {}", request.getHeaders());
//			logger.info("Content Type : {}", request.getHeaders().get("Content-Type"));
//			logger.info("Date         : {}", request.getHeaders().get(Constanta.DATE));
//			logger.info("Signature    : {}", request.getHeaders().get(Constanta.SIGNATURE));
//			logger.info("==========================request end===================================================");
		}
	}
}
