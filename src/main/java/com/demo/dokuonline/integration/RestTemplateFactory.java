package com.demo.dokuonline.integration;

import java.util.Collections;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Component
public class RestTemplateFactory implements InitializingBean {

	@Autowired
	CloseableHttpClient httpClient;

	private RestTemplate notificationTemplate;

	@Value("${notification.uri}")
	private String notificationHost;

	@Value("${service.rest.timeout.read}")
	private int readTimeout;

	@Value("${service.rest.timeout.connect}")
	private int connectTimeout;

	@Bean("notificationTemplate")
	@Primary
	public RestTemplate notificationTemplate() {
		return notificationTemplate;
	}

	@Autowired
	RequestSignatureInterceptor requestSignatureInterceptor;

	@Override
	public void afterPropertiesSet() {
//		HttpHost host = new HttpHost(baseURI, port, protocol);
		final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setHttpClient(httpClient);
		clientHttpRequestFactory.setReadTimeout(readTimeout);
		clientHttpRequestFactory.setConnectTimeout(connectTimeout);
//		final ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactoryBasicAuth(host);
		notificationTemplate = new RestTemplate(clientHttpRequestFactory);
		notificationTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(this.notificationHost));
		//notificationTemplate.setMessageConverters(Collections.singletonList(new MappingJackson2HttpMessageConverter()));
		notificationTemplate.setInterceptors(Collections.singletonList(requestSignatureInterceptor));

//		restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(userAPI, passwordAPI));
//		restTemplate.getInterceptors().add(new RequestResponseLoggingInterceptor());
	}
}
