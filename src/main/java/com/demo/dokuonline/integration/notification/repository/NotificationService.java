package com.demo.dokuonline.integration.notification.repository;

import com.demo.dokuonline.employee.endpoint.model.EmployeeModel;

public interface NotificationService {
	public String greeting(EmployeeModel data);
}
