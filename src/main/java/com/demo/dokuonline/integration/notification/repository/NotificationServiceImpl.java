package com.demo.dokuonline.integration.notification.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.demo.dokuonline.employee.endpoint.model.EmployeeModel;

@Service
public class NotificationServiceImpl implements NotificationService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String pathUri = "/message";

	RestTemplate restTemplate;

	public NotificationServiceImpl(@Qualifier("notificationTemplate") RestTemplate restTemplate) {
		// TODO Auto-generated constructor stub
		this.restTemplate = restTemplate;
	}

	@Override
	public String greeting(EmployeeModel data) {
		// TODO Auto-generated method stub
		ResponseEntity<String> result = restTemplate.postForEntity(pathUri, data, String.class);
		
		return result.getBody();
	}
}
